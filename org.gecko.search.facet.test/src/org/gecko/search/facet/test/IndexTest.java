/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.search.facet.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.facet.DrillDownQuery;
import org.apache.lucene.facet.FacetField;
import org.apache.lucene.facet.FacetResult;
import org.apache.lucene.facet.Facets;
import org.apache.lucene.facet.FacetsCollector;
import org.apache.lucene.facet.FacetsCollector.MatchingDocs;
import org.apache.lucene.facet.FacetsConfig;
import org.apache.lucene.facet.LabelAndValue;
import org.apache.lucene.facet.taxonomy.FacetLabel;
import org.apache.lucene.facet.taxonomy.FastTaxonomyFacetCounts;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.facet.taxonomy.TaxonomyReader.ChildrenIterator;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyWriter;
import org.apache.lucene.index.BinaryDocValues;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.DocValues;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.gecko.core.tests.AbstractOSGiTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;

@RunWith(MockitoJUnitRunner.class)
public class IndexTest {
//
  private static final String LINEAGE = "lineage";
  private final Directory indexDir = new RAMDirectory();
  private final Directory taxoDir = new RAMDirectory();
//
//	/**
//	 * Creates a new instance.
//	 * @param bundleContext
//	 */
//	public IndexTest() {
//	}
//
//	@Test
//	public void basicTest() throws InterruptedException, IOException {
//		FacetsConfig config = new FacetsConfig();
//		
//	    config.setIndexFieldName(LINEAGE,  LINEAGE);
//	    config.setHierarchical(LINEAGE, true);
//	    config.setRequireDimensionDrillDown(LINEAGE, true);
//		
//		IndexWriter indexWriter = new IndexWriter(indexDir, new IndexWriterConfig(
//			        new StandardAnalyzer()).setOpenMode(OpenMode.CREATE));
//
//	    // Writes facet ords to a separate directory from the main index
//	    DirectoryTaxonomyWriter taxoWriter = new DirectoryTaxonomyWriter(taxoDir);
//
//	    Document doc = new Document();
//	    doc.add(new StringField("id", "1", Store.YES));
//	    doc.add(new StringField("title", "Project 1", Store.YES));
//	    indexWriter.addDocument(config.build(taxoWriter, doc));
//
//	    doc = new Document();
//	    doc.add(new StringField("id", "2", Store.YES));
//	    doc.add(new StringField("title", "WP 1", Store.YES));
//	    doc.add(new FacetField(LINEAGE, "WP 1"));
//	    indexWriter.addDocument(config.build(taxoWriter, doc));
//
//	    doc = new Document();
//	    doc.add(new StringField("id", "3", Store.YES));
//	    doc.add(new StringField("title", "WP 2", Store.YES));
//	    doc.add(new FacetField(LINEAGE, "WP 2"));
//	    indexWriter.addDocument(config.build(taxoWriter, doc));
//
//	    doc = new Document();
//	    doc.add(new StringField("id", "4", Store.YES));
//	    doc.add(new StringField("title", "WP 3", Store.YES));
//	    doc.add(new FacetField(LINEAGE, "WP 3"));
//	    indexWriter.addDocument(config.build(taxoWriter, doc));
//
//	    doc = new Document();
//	    doc.add(new StringField("id", "5", Store.YES));
//	    doc.add(new StringField("title", "Task 1 in WP 2", Store.YES));
//	    doc.add(new FacetField(LINEAGE, "WP 2", "Task 1 in WP 2"));
//	    indexWriter.addDocument(config.build(taxoWriter, doc));
//
//	    doc = new Document();
//	    doc.add(new StringField("id", "6", Store.YES));
//	    doc.add(new StringField("title", "Task 2 in WP 2", Store.YES));
//	    doc.add(new FacetField(LINEAGE, "WP 2", "Task 2 in WP 2"));
//	    indexWriter.addDocument(config.build(taxoWriter, doc));
//	    
//	    doc = new Document();
//	    doc.add(new StringField("id", "7", Store.YES));
//	    doc.add(new StringField("title", "Task 1.1 in WP 2", Store.YES));
//	    doc.add(new FacetField(LINEAGE, "WP 2", "Task 1 in WP 2", "Task 1.1 in WP 2"));
//	    indexWriter.addDocument(config.build(taxoWriter, doc));
//	    
//	    indexWriter.commit();
//	    taxoWriter.commit();
//	    
//	    DirectoryReader indexReader = DirectoryReader.open(indexDir);
//	    IndexSearcher searcher = new IndexSearcher(indexReader);
//	    TaxonomyReader taxoReader = new DirectoryTaxonomyReader(taxoDir);
//
//	    FacetsCollector fc = new FacetsCollector();
//
//	    // MatchAllDocsQuery is for "browsing" (counts facets
//	    // for all non-deleted docs in the index); normally
//	    // you'd use a "normal" query:
//	    TopDocs search = FacetsCollector.search(searcher, new TermQuery(new Term("id", "7")), 10, fc);
//
//	    Document document = indexReader.document(search.scoreDocs[0].doc);
//	    System.out.println("Result: " + document.get("id") + " " + document.get("title"));
//	    
//	    for (MatchingDocs md : fc.getMatchingDocs()) {
//	    	BinaryDocValues binary = DocValues.getBinary(md.context.reader(), LINEAGE);
//	    	
//	    	binary.binaryValue().
//	    	
//	    	DocIdSetIterator it = md.bits.iterator();
//	    	int docId = it.nextDoc();
//	    	while(docId != DocIdSetIterator.NO_MORE_DOCS) {
//	    		System.out.println("Facet DocId " + docId);
//	    		FacetLabel path = taxoReader.getPath(docId);
//	    		for(String element : path.components) {
//					System.out.println("path element " +  element);
//				}
//	    		docId = it.nextDoc();
//	    	}
//	    	
//	    	
//	    	System.out.println("Is Top Level " + md.context.isTopLevel);
//	    	System.out.println("ord " + md.context.ord);
//	    	System.out.println("ordInParent " + md.context.ordInParent);
//	    	
//	    	md.context.children().forEach(irc -> {
//	    		System.out.println("Children ");
//	    		System.out.println("Is Top Level " + irc.isTopLevel);
//	    		System.out.println("docBaseInParent " + irc.docBaseInParent);
//	    		System.out.println("ordInParent " + irc.ordInParent);
//	    		irc.children().forEach(irc2 -> {
//	    			System.out.println("Children 2");
//	    			System.out.println("Is Top Level " + irc2.isTopLevel);
//	    			System.out.println("docBaseInParent " + irc2.docBaseInParent);
//	    			System.out.println("ordInParent " + irc2.ordInParent);
//	    			
//	    		});
//	    	});
//	    	
//	    	FacetLabel path = taxoReader.getPath(md.context.ord);
//			for(String element : path.components) {
//				System.out.println("path element " +  element);
//			}
//			
//		}
	    
//	    Facets lineage = new FastTaxonomyFacetCounts(LINEAGE, taxoReader, config, fc);
//	    for (FacetResult facetResult : lineage.getAllDims(10)) {
//	    	System.out.println("DIM " + facetResult.dim + " child: " + facetResult.childCount);
//	    	for (LabelAndValue lv : facetResult.labelValues) {
//	    		System.out.println("label " +  lv.label);
//	    		System.out.println("count " +  lv.value.toString());
//			}
//	    	for(String element : facetResult.path) {
//				System.out.println("path element " +  element);
//			}
//	    	
//	    	ChildrenIterator children = taxoReader.getChildren(0);
//	    	int childOrd = children.next();
//	    	while(childOrd != TaxonomyReader.INVALID_ORDINAL) {
//	    		FacetLabel thePath = taxoReader.getPath(childOrd);
//	    		for(String element : thePath.components) {
//	    			System.out.println(element);
//	    		}
//	    		childOrd = children.next();
//	    	}
//
//		}
	    
//	    List<MatchingDocs> matchingDocs = fc.getMatchingDocs();
//	    matchingDocs.forEach(md -> {
//	    	try {
//		    	FacetLabel path = taxoReader.getPath(md.context.ord);
//				for(String element : path.components) {
//					System.out.println(element);
//				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
//	    });
	    
//	    indexReader.close();
//	    taxoReader.close();
//	    
//	    indexWriter.close();
//	    taxoWriter.close();
//		
//		
//	}
	
	/**
	 * @param tempFolder
	 */
	private void delete(File file) {
		if(file.exists()) {
			if(!file.isFile()) {
				Arrays.asList(file.listFiles()).forEach(this::delete);
			}
			file.delete();
		}
	}
}